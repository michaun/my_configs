# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi


# Put your fun stuff here.
alias gitk='gitk --all --branches --tags --remotes'
alias gdiff='git diff --no-index --word-diff=color --word-diff-regex=.'
alias ta='tmux attach'

stty -ixon

. /etc/profile.d/bash_completion.sh

export PS1='\[\033[01;32m\][$?][\D{%FT%T}][\u@\h:\l]\[\033[01;33m\][\j][\w]$\[\033[00m\] '
export HISTCONTROL='ignorespace'
export PATH=~/.local/bin:$PATH
export EDITOR=vim
export HISTSIZE=-1
export HISTFILESIZE=-1

# mkdir -p $HOME/term_logs
# [ "$(ps -ocommand= -p $PPID | awk '{print $1}')" == 'script' ] || (script -f $HOME/term_logs/$(date +"%Y%m%d_%H%M%S").log)
# note: view with less -R <file>

# REMEMBER ABOUT ME
# grep -E "(ctrl|caps):" /usr/share/X11/xkb/rules/base.lst
# The following fits better as XkbOption (Option "XkbOptions" "caps:none")
# so that there is nocaps out of the box when X starts: 
#   setxkbmap -option caps:none
# or (for emacs)
# setxkbmap -option "ctrl:nocaps"
# localectl set-x11-keymap pl,us '' '' ctrl:nocaps
