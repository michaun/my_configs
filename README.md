# my_configs

## What is it?

This is a collection of configuration files I use.
It is mostly here for myself (for whenever I have to reconfigure my env).
If you find any of it of any value - feel free to use it (at your own risk).
