set encoding=utf-8 " utf8 encoding
scriptencoding utf-8 " utf8 encoding
syntax on " syntax highlight
set nocompatible " use vim extensions
set confirm " confirm operations like writing and quiting
set ruler " show line and column of cursor
set backup " create backup copy
set backupdir=~/.vimbackups// "where to create backup copy + fulldir with //
set directory=~/.vimswaps// "where to create *,swp files + fulldir with //
set wildmenu "show autocompletion options
" replaced by airline plugin
" set laststatus=2 " always(2) show status line
" set statusline=%<%F%h%m%r%h%w%y\ %{&ff}\ %{strftime(\"%d/%m/%Y-%H:%M\")}\ %=\ col:%c%V\ ascii:%b\ pos:%o\ lin:%l\/%L\ %P
set number " show line numbers
set showcmd " show command that is being entered
set hlsearch " highlight search
set scrolloff=3 "spaces between cursor and end of the window
set textwidth=0 " please dont auto-newline me while i type, thank you
let g:leave_my_textwidth_alone = 1 " avoid text wrapping altogether
set hidden " do not ask for saving when awitching buffers
set guioptions-=T " no toolbar in gvim
set smarttab " be smart
set shiftwidth=4 " indent
set softtabstop=4 " indent
set tabstop=4 " indent
set expandtab " change tabs to spaces
set autoindent " indent when needed
set backspace=indent,eol,start " allow backspacing over everything in insert mode
set incsearch "incremental search
set cindent " C indenting on
set cursorline "highlight line with cursor
set cursorcolumn "highlight column with cursor
set colorcolumn=89 " highlight margin
set viminfo+=n~/.vim/viminfo " place viminfo here
set nowrap "do not wrap lines at the end of the screen
set title "set window title
let g:mapleader = ',' " change vim leader
set foldcolumn=2 " folding level
set foldmethod=indent " folding method
set nofoldenable        "dont fold by default
set guicursor+=a:blinkon0 " do not blink
set wildignore+=venv/**,*/node_modules/* " do not search through venv and node_modules (e.g. :vimgrep)

colorscheme habamax
au Colorscheme habamax hi MatchParen ctermfg=yellow ctermbg=black cterm=None

noremap ; :
noremap q; q:
" noremap ,, :nohlsearch<CR>
inoremap jk <Esc>

noremap <Left> <NOP>
noremap <Right> <NOP>
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <F1> <NOP>
inoremap <F1> <NOP>

" noremap <F9> :bp<CR>
" noremap <F10> :bn<CR>
" noremap <F11> gT
" noremap <F12> gt

noremap <F11> :bp<CR>
noremap <F12> :bn<CR>
noremap gB :bp<CR>
noremap gb :bn<CR>

" [mk]session options
set ssop-=options    " do not store global and local values in a session
set ssop-=folds      " do not store folds

cnoremap w!! w !sudo tee > /dev/null %

" switched to vim8 plugin system
" """ pathogen """
" runtime bundle/vim-pathogen/autoload/pathogen.vim
" execute pathogen#infect()
" execute pathogen#helptags()
" """"""""""""""""

filetype plugin indent on " supposedly this should go after plugins are loaded - don't know if this is true and don't care
syntax on " highlight syntax

""" plugin stuff """
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#battery#enabled = 1
let g:airline#extensions#branch#displayed_head_limit = 10
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_section_y = airline#section#create(['%{strftime("[%H:%M %d.%m.%Y]")}', ' ', 'ffenc'])

let g:jedi#show_call_signatures = "2"

nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-h> :NERDTreeFind<CR>
let NERDTreeIgnore = ['\~$', '__pycache__', 'node_modules']
let g:NERDTreeWinSize=50

let g:python_highlight_all = 1

"map <C-c> <Plug>(easymotion-prefix)

" .vim/pack/plugins/start/
" ├── ale # https://github.com/dense-analysis/ale.git
" ├── battery.vim # https://github.com/lambdalisue/battery.vim.git
" ├── ctrlp # https://github.com/ctrlpvim/ctrlp.vim.git
" ├── jedi-vim # https://github.com/davidhalter/jedi-vim.git
" ├── nerdtree # https://github.com/preservim/nerdtree.git
" ├── python-syntax # https://github.com/vim-python/python-syntax.git
" ├── vim-airline # https://github.com/vim-airline/vim-airline.git
" ├── vim-easymotion # https://github.com/easymotion/vim-easymotion.git
" ├── vim-fugitive # https://github.com/tpope/vim-fugitive.git
" ├── vim-go # https://github.com/fatih/vim-go
" ├── vim-ingo-library # https://github.com/inkarkat/vim-ingo-library (dep for vim-mark)
" ├── vim-mark # https://github.com/inkarkat/vim-mark
" ├── vim-surround # https://github.com/tpope/vim-surround.git
" └── zoomwintab.vim # https://github.com/troydm/zoomwintab.vim.git

