(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package nyan-mode
  :ensure t
  :init
  (nyan-mode 1))

(use-package which-key
  :ensure t
  :init
  (setq which-key-idle-delay 0.5)
  (which-key-setup-side-window-bottom)
  (which-key-mode))

(use-package keycast
  :ensure t
  :init
  (keycast-tab-bar-mode))

(use-package ace-window
  :ensure t
  :init
  (global-set-key (kbd "C-z") 'ace-window))

(use-package avy
  :ensure t
  :init
  (global-set-key (kbd "C-c SPC") 'avy-goto-char-timer)
  (global-set-key (kbd "C-c C-SPC") 'avy-goto-char-timer))

(use-package company
  :ensure t
  :init
  (company-mode)
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 1)
  (setq company-selection-wrap-around t)
  (add-hook 'after-init-hook 'global-company-mode))

(use-package neotree
  :ensure t
  :init
  (global-set-key (kbd "<f7>") 'neotree-find)
  (global-set-key (kbd "<f8>") 'neotree-toggle)
  (setq neo-window-fixed-size nil)
  (setq neo-window-width 55))

(use-package markdown-mode
  :ensure t
  :init
  (markdown-mode))

(use-package eglot
  :ensure t
  :hook (python-mode . eglot-ensure))

(use-package highlight-indentation
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'highlight-indentation-mode))

; (use-package elpy
;   :ensure t
;   :init
;   (elpy-enable))

(use-package ido
  :ensure t
  :init
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (ido-mode 1))

(use-package icomplete
  :ensure t
  :init
  (fido-mode 1))

(use-package winner
  :ensure t
  :init
  (winner-mode))  ; C-c left/right - win history

(use-package tab-bar
  :ensure t
  :init
  (setq tab-bar-separator " ")
  (setq tab-bar-new-button-show nil)
  (setq tab-bar-close-button-show nil)
  (set-face-attribute 'tab-bar nil :background "#0a2832" :foreground "8ab1bf" :box nil)
  (set-face-attribute 'tab-bar-tab nil :background "#081f27" :foreground "#76dcff" :box nil)
  (set-face-attribute 'tab-bar-tab-inactive nil :background "#081f27" :foreground "#5c47ff" :box nil))

(use-package tab-line
  :ensure t
  :init
  (global-tab-line-mode)
  (setq tab-line-separator " ")
  (setq tab-line-new-button-show nil)
  (setq tab-line-close-button-show nil)
  (set-face-attribute 'tab-line nil :background "#0a2832" :foreground "8ab1bf" :box nil :height 98)
  (set-face-attribute 'tab-line-tab nil :background "#05151a" :foreground "#332864" :box nil)
  (set-face-attribute 'tab-line-tab-current nil :background "#081f27" :foreground "#76dcff" :box nil)
  (set-face-attribute 'tab-line-tab-inactive nil :background "#081f27" :foreground "#5c47ff" :box nil))

(use-package paren
  :ensure t
  :init
  (show-paren-mode 1))

(use-package delsel
  :ensure t
  :init
  (delete-selection-mode))

(use-package org
  :ensure t
  :init
  (org-mode)
  (add-to-list 'org-agenda-files "~/org/"))

(use-package display-line-numbers
  :ensure t
  :init
  (global-display-fill-column-indicator-mode)
  (setq-default display-fill-column-indicator-column 80))

(use-package display-line-numbers
  :ensure t
  :init
  (global-display-line-numbers-mode 1))

(use-package calendar
  :ensure t
  :init
  (setq-default calendar-week-start-day 1)
  (setq-default calendar-date-style 'european))

(use-package time
  :ensure t
  :init
  (setq-default display-time-format "%T  %d/%m/%Y  %A")
  (display-time-mode 1))

(use-package battery
  :ensure t
  :init
  (display-battery-mode 1))

(use-package which-func
  :ensure t
  :init
  (which-function-mode 1))

;;;;;;

; global
(setq-default use-dialog-box nil)
(setq-default inhibit-startup-screen t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(toggle-frame-maximized)

; colors
(set-face-background 'mode-line "#0a2832")
(set-face-background 'mode-line-inactive "#0a2832")
(set-face-foreground 'mode-line "#ffffff")
(set-background-color "black")
(add-to-list 'default-frame-alist '(background-color . "black"))
(set-foreground-color "white")
(add-to-list 'default-frame-alist '(foreground-color . "white"))

; backup files
(setq-default backup-directory-alist `(("." . "~/.emacssaves")))
(setq-default backup-by-copying t)
(setq-default delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)

; general look'n'feel
(blink-cursor-mode 0)
(setq-default c-basic-offset 2)
(setq-default sentence-end-double-space nil)
(setq-default column-number-mode t)
(setq-default truncate-lines t)
(setq-default tab-width 2)
(setq-default indent-tabs-mode nil)
(setq-default frame-title-format '("%b      %f  " global-mode-string))
(setq-default echo-keystrokes 0.01)

; shortcuts
(global-set-key (kbd "M-f") 'forward-same-syntax)
(global-set-key (kbd "M-b") (lambda () (interactive) (forward-same-syntax -1)))
(global-set-key (kbd "C-x C-b") 'ibuffer-other-window)
(global-set-key (kbd "<f11>") 'previous-buffer)
(global-set-key (kbd "<f12>") 'next-buffer)
(global-set-key (kbd "C-c $") 'toggle-truncate-lines)
(global-unset-key [insert])

; allow "dangerous" comnmands
(put 'scroll-left 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

; custom defuns
;;;;; display file path in header line [https://www.emacswiki.org/emacs/HeaderLine]
(defun with-face (str &rest face-plist)
  (propertize str 'face face-plist))

(defun sl/make-header ()
  ""
  (let* ((sl/full-header (abbreviate-file-name buffer-file-name))
         (sl/header (file-name-directory sl/full-header))
         (sl/drop-str "[...]"))
    (if (> (length sl/full-header)
           (window-body-width))
        (if (> (length sl/header)
               (window-body-width))
            (progn
              (concat (with-face sl/drop-str
                                 :background "blue"
                                 :weight 'bold
                                 )
                      (with-face (substring sl/header
                                            (+ (- (length sl/header)
                                                  (window-body-width))
                                               (length sl/drop-str))
                                            (length sl/header))
                                 ;; :background "red"
                                 :weight 'bold
                                 )))
          (concat (with-face sl/header
                             ;; :background "red"
                             :foreground "#8fb28f"
                             :weight 'bold
                             )))
      (concat (with-face sl/header
                         ;; :background "green"
                         ;; :foreground "black"
                         :weight 'bold
                         :foreground "#8fb28f"
                         )
              (with-face (file-name-nondirectory buffer-file-name)
                         :weight 'bold
                         ;; :background "red"
                         )))))

(defun sl/display-header ()
  (setq header-line-format
        '("" ;; invocation-name
          (:eval (if (buffer-file-name)
                     (sl/make-header)
                   "%b")))))

(add-hook 'buffer-list-update-hook
          'sl/display-header)
;;;;; end display file path in header line [https://www.emacswiki.org/emacs/HeaderLine]

